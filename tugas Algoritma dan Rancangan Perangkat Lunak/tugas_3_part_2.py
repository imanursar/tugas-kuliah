
#fungsi rekursif
def mainfunction(n,m):
    if n <= 1:
        return n
    res = 0
    i = 1
    while i<=m and i<=n:
        res = res + mainfunction(n-i, m)
        i = i + 1
    return res

# Returns number of ways to reach s'th stair
def return_number(s,m):
    return mainfunction(s+1, m)


# main program

print("\n"+"input jumlah step")
s = int(input("input: "))
print("\n")

print("\n"+"input maksimum step dengan nilai yang sama dengan jumlah step bila tidak ada batas maksimum step")
print("input maksimum step")
m = int(input("input: "))
print("\n")

# s,m = 5,5
print ("Number of ways =", return_number(s, m))
