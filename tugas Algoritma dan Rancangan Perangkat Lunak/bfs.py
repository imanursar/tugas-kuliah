graph_1 = {1: set([3, 11]),
         3: set([70]),
         11: set([15,21,20,90]),
         15: set([21]),
         20: set([32]),
         21: set([30]),
         30: set([60]),
         32: set([42]),
         42: set([52]),
         52: set([62]),
         62: set([72]),
         72: set([82]),
         82: set([92]),
         92: set([100]),
         21: set([31]),
         31: set([41]),
         41: set([51]),
         51: set([61]),
         61: set([71]),
         71: set([81]),
         81: set([91]),
         91: set([100]),
         60: set([70]),
         70: set([80]),
         80: set([90]),
         90: set([100]),
         100: set([])}

class BSF(object):
    def __init__(self):
        print("BSF")
        self.output = []

    def main(self, graph, start):
        print("\nsemua jalur yang bisa dilewati")
        visited, queue = set(), [start]
        while queue:
            vertex = queue.pop(0)
            if vertex not in visited:
                if (vertex not in self.output):
                    self.output.append(vertex)
                visited.add(vertex)
                queue.extend(sorted(graph[vertex] - visited))
        return self.output

    def paths(self,graph, start, goal):
        # print("all paths")
        queue = [(start, [start])]
        while queue:
            (vertex, path) = queue.pop(0)
            for next in graph[vertex] - set(path):
                if next == goal:
                    yield path + [next]
                else:
                    queue.append((next, path + [next]))

    def shortest(self, graph, start, goal):
        try:
            return next(bsf.paths(graph, start, goal))
        except StopIteration:
            return None

if __name__ == '__main__':
    bsf=BSF()
    graph = graph_1
    start = 1
    end = 100
    print(bsf.main(graph, start))
    print("\nsemua kemungkinan jalur yang bisa dilewati dari %s hingga %s" %(start,end))
    print(list(bsf.paths(graph, start, end)))
    print("\njalur terpendek yang dilewati dari %s hingga %s" %(start,end))
    print(bsf.shortest(graph, start, end))
