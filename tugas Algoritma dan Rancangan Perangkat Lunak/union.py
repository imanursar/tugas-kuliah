graph_1 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(14, 3, 5),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

graph_2 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(5, 1, 8),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(14, 3, 5),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

graph_3 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(5, 1, 8),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(14, 3, 5),
(6, 3, 6),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

graph_4 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(5, 1, 8),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(14, 3, 5),
(6, 3, 6),
(4, 3, 8),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

class Union(object):

    def __init__(self):
        print("union")
        print("detected cycle in graph\n")
        self.parent = dict()
        self.rank = dict()

    def make_set(self,vertice):
        self.parent[vertice] = vertice
        self.rank[vertice] = 0

    def find(self,vertice):
        if self.parent[vertice] != vertice:
            self.parent[vertice] = Union.find(self,self.parent[vertice])
        return self.parent[vertice]

    def union(self,vertice1, vertice2):
        root1 = Union.find(self,vertice1)
        root2 = Union.find(self,vertice2)
        if (root1 != root2):
            if self.rank[root1] > self.rank[root2]:
                self.parent[root2] = root1
            else:
                self.parent[root1] = root2
            if (self.rank[root1] == self.rank[root2]):
                self.rank[root2] += 1

    def main(self,graph):
        for vertice in graph['nodes']:
            Union.make_set(self,vertice)
            mst = []
        edges = list(graph['edges'])
        edges.sort()
        for edge in edges:
            weight, vertice1, vertice2 = edge
            if Union.find(self,vertice1) != Union.find(self,vertice2):
                Union.union(self,vertice1, vertice2)
                mst.append(edge)
                print("not cycle= "+ str(edge))
            else:
                print("cycle= "+ str(edge))


if __name__ == '__main__':
    union=Union()
    union.main(graph_1)
    print("\n")
    union=Union()
    union.main(graph_2)
    print("\n")
    union=Union()
    union.main(graph_3)
    print("\n")
    union=Union()
    union.main(graph_4)
