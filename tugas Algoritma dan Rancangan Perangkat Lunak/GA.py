import numpy as np

#Initial Population
print("masukkan jumlah kromosom pada suatu populasi (gunakan nilai genap)")
k=int(input("\njumlah kromosom:"))
print("\nmasukkan jumlah gen pada suatu kromosom")
g=int(input("\njumlah gen:"))
print("\n")
input = np.zeros((k,g))

for i in range(0,k):
    input[i] = np.random.randint(2, size=g)
    while(sum(input[i]) > 2):
        input[i] = np.random.randint(2, size=g)

# fitness function
def fit(x):
    return(sum(x.T))

#selection_parent
def selection_parent(x,k,g):
    fit_res = fit(x)
    fit_res = fit_res/sum(fit_res)
    all = 0
    for i in range(0,len(fit_res)):
        all = all + fit_res[i]
        fit_res[i] = all

    inter = np.zeros((k,2))
    inter[0,:] = [0,fit_res[0]]

    for i in range(1,k):
        inter[i,:] = [fit_res[i-1],fit_res[i]]

    rando = np.random.random(k)
    draft = np.zeros((k))

    m = 0
    for j in range(0,k):
        for i in range(0,k):
            if (rando[j] > inter[i,0] and rando[j] < inter[i,1]):
                draft[m] = i
                m = m+1

    parent_1 = np.zeros((int(k/2),g))
    parent_2 = np.zeros((int(k/2),g))
    for i in range(0,int(k/2)):
        parent_1[i] = x[int(draft[np.random.randint(0,high=k)])]
        parent_2[i] = x[int(draft[np.random.randint(0,high=k)])]

    return(parent_1,parent_2)

#crossover
def crossover(k,g,cro_1,cro_2):
    for i in range(0,int(k/2)):
        if (g%2 == 0):
            cro_1[i,int(g/2):len(cro_1)] = cro_1[i,int(g/2):len(cro_1)] + cro_2[i,int(g/2):len(cro_1)]
            cro_2[i,int(g/2):len(cro_1)] = cro_1[i,int(g/2):len(cro_1)] - cro_2[i,int(g/2):len(cro_1)]
            cro_1[i,int(g/2):len(cro_1)] = cro_1[i,int(g/2):len(cro_1)] - cro_2[i,int(g/2):len(cro_1)]
        else:
            cro_1[i,int((g-1)/2):len(cro_1)] = cro_1[i,int((g-1)/2):len(cro_1)] + cro_2[i,int((g-1)/2):len(cro_1)]
            cro_2[i,int((g-1)/2):len(cro_1)] = cro_1[i,int((g-1)/2):len(cro_1)] - cro_2[i,int((g-1)/2):len(cro_1)]
            cro_1[i,int((g-1)/2):len(cro_1)] = cro_1[i,int((g-1)/2):len(cro_1)] - cro_2[i,int((g-1)/2):len(cro_1)]

    result = np.concatenate((cro_1, cro_2), axis=0)
        # print(result)
    return(result)

#mutation
def mutation(g,x):
    for j in range(0,k):
        if (g%2 == 0):
            mut = np.random.randint(1,high=int(g/2))
            idx = np.random.randint(0,high=g,size=mut)
            prob = np.random.randint(0,high=10)
            if(prob < 1):
                for i in range(0,len(idx)):
                    if(x[j,idx[i]] == 0):
                        x[j,idx[i]] = 1
                    else:
                        x[j,idx[i]] = 0
        else:
            mut = np.random.randint(1,high=int((g+1)/2))
            idx = np.random.randint(0,high=g,size=mut)
            prob = np.random.randint(0,high=10)
            if(prob < 1):
                for i in range(0,len(idx)):
                    if(x[j,idx[i]] == 0):
                        x[j,idx[i]] = 1
                    else:
                        x[j,idx[i]] = 0
    return(x)

#selection survivor
def survivor(input,edit):
    output = np.array(edit)
    return(output)

print("\n\n\ntest area")
print("\ninput")
print(input)
print("\nfit")
print(fit(input))
print("\n")

n = 1000
b = 0
print("generation= %d" %b,end='')
print(" fitness= %d" %max(fit(input)))

for i in range(1,n):
    x,y = selection_parent(input,k,g)
    co = crossover(k,g,x,y)
    res = mutation(g,co)
    input = survivor(input,res)
    print("generation= %d" %i,end='')
    print(" fitness= %d" %max(fit(input)))
    if (max(fit(input)) == 5):
        print("finish")
        break
