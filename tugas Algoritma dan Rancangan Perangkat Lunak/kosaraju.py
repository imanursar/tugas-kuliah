output = []

def dfs(graph, start, visited=None):
    if visited is None:
        visited = set()
        output.append(start)
    visited.add(start)
    for next in sorted(graph[start] - visited):
        # print(sorted(graph[start] - visited))
        print(next)
        print(visited)
        if (next not in output):
            output.append(next)
        # if (next in visited):
        #     print(next)
        dfs(graph, next, visited)
    return output

def reverse(h_normal):
    h_reversed = dict()
    for i in h_normal:
        for j in h_normal[i]:
            if j not in h_reversed:
                h_reversed[j] = set([i])
            else:
                h_reversed[j].add(i)
    return(h_reversed)

# def dfs_paths(graph, start, goal, path=None):
#     if path is None:
#         path = [start]
#     if start == goal:
#         yield path
#     for next in graph[start] - set(path):
#         yield from dfs_paths(graph, next, goal, path + [next])

# graph = {'A': set(['B', 'C']),
#          'B': set(['D', 'E']),
#          'C': set(['A', 'E']),
#          'D': set(['B', 'E']),
#          'E': set(['B', 'C', 'D', 'F']),
#          'F': set(['D', 'E'])}

# graph = {'A': set(['B', 'C']),
#          'B': set(['D']),
#          'C': set(['D']),
#          'D': set([])}

# graph = {'A': set(['B', 'D', 'E']),
#          'B': set(['C', 'F']),
#          'C': set([]),
#          'D': set(['G']),
#          'E': set(['G']),
#          'F': set([]),
#          'G': set(['H']),
#          'H': set(['I']),
#          'I': set([])}

graph = {'A': set(['B']),
         'B': set(['C','D']),
         'C': set(['A']),
         'D': set(['E']),
         'E': set(['F']),
         'F': set(['D']),
         'G': set(['F','H']),
         'H': set(['I']),
         'I': set(['J']),
         'J': set(['G','K'])}


output_1 = dfs(graph, 'A')
# print(output_1[::-1])
#
# graph_rev = reverse(graph)
# # print(graph_rev)
#
# output_2 = dfs(graph_rev, 'A')
print(output_1)
# print(list(dfs_paths(graph, 'A', 'I'))) # [['C', 'F'], ['C', 'A', 'B', 'E', 'F']]
