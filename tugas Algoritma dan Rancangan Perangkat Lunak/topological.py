graph_1 = {5: set([0, 2]),
           4: set([0, 1]),
           2: set([3]),
           3: set([1]),
           0: set([]),
           1: set([])
           }

graph_2 = {1: set([2, 3]),
           2: set([3, 4]),
           3: set([4, 5]),
           4: set([]),
           5: set([])}

graph_3 = {0: set([1, 3]),
           1: set([2, 3]),
           2: set([3, 4, 5]),
           3: set([4, 5]),
           4: set([5]),
           5: set([])}

graph_4 = {0: set([1, 2]),
           1: set([3]),
           2: set([3]),
           3: set([4]),
           4: set([])}

graph_5 = {0: set([1, 5]),
           1: set([7]),
           2: set([]),
           3: set([2, 4, 7, 8]),
           4: set([8]),
           5: set([]),
           6: set([0, 2]),
           7: set([]),
           8: set([2]),
           9: set([4])
           }

output = []

def main(graph, start, visited=None):
    if visited is None:
        visited = set()
        output.append(start)
    visited.add(start)

    for next in sorted(graph[start] - visited):
        if (next not in output):
            output.append(next)
        main(graph, next, visited)
    return output

def main_paths(graph, start, goal, path=None):
    if path is None:
        path = [start]
    if start == goal:
        yield path
    for next in graph[start] - set(path):
        yield from main_paths(graph, next, goal, path + [next])




run= 0
graph = graph_3
print("all path")
print(main(graph, run))
print("\n")

print("path")
for i in range(min(graph)+1, max(graph)+1):
    print(list(main_paths(graph, run, i)))

    if ( i == min(graph)+1):
        all_1 = list(main_paths(graph, run, i))
    else:
        all_2 = list(main_paths(graph, run, i))
        if (len(all_1[0]) > len(all_2[0])):
            all_1 = all_2

print("\nshort part")
print(all_1[0])
