import numpy as np

class floyd_warshall(object):
    def main(S):
        nxt  = [[0]   * len(S) for i in range(len(S))]

        dist = np.array(S)
        for z in range(0,len(S)):
            dist[z,z] = 0

        for q in range(0,len(S)):
            for w in range(0,len(S)):
                if (S[q,w]<999):
                    nxt[q][w] = w

        for k in range(0,len(dist)):
            for i in range(0,len(dist)):
                for j in range(0,len(dist)):
                    if (dist[i,j] > dist[i,k] + dist[k,j]):
                        dist[i,j] = dist[i,k] + dist[k,j]
                        nxt[i][j]  = nxt[i][k]
        return(dist,nxt)

    def path(S,nxt,i,j):
        i = i -1
        j = j -1
        print("pasangan     jarak    jalur")
        if i != j:
            path = [i]
            while path[-1] != j:
                path.append(nxt[path[-1]][j])
            print("%d → %d     %4d        %s"% (i + 1, j + 1, S[i][j],' → '.join(str(p + 1) for p in path)))


if __name__ == '__main__':
    #buat matriks floyd_warshall
    n = 4
    S = np.ones((n,n))*999

    #isi matriks floyd_warshall
    S[0,2]=-2
    S[1,0]=4
    S[1,2]=3
    S[2,3]=2
    S[3,1]=-1

    #hasil matriks floyd_warshall
    print("matriks awal\n")
    print(S)
    dist,nxt = floyd_warshall.main(S)
    print("\nmatriks floyd_warshall\n")
    print(dist)
    print("\n")
    #mencari jalur terpendek
    floyd_warshall.path(dist,nxt,1,2)
