graph_1 = [
  [0,1,4],
  [0,7,8],
  [1,2,8],
  [1,7,11],
  [2,3,7],
  [2,5,2],
  [2,8,4],
  [3,4,9],
  [3,5,14],
  [4,5,10],
  [5,6,2],
  [6,8,6],
  [6,7,1],
  [7,8,7]
]

graph_2 = [
  [0,1,4],
  [0,7,8],
  [1,2,8],
  [1,7,11],
  [1,8,5],
  [2,3,7],
  [2,5,2],
  [2,8,4],
  [3,4,9],
  [3,5,14],
  [4,5,10],
  [5,6,2],
  [6,8,6],
  [6,7,1],
  [7,8,7]
]

graph_3 = [
  [0,1,4],
  [0,7,8],
  [1,2,8],
  [1,7,11],
  [1,8,5],
  [2,3,7],
  [2,5,2],
  [2,8,4],
  [3,4,9],
  [3,5,14],
  [3,6,6],
  [4,5,10],
  [5,6,2],
  [6,8,6],
  [6,7,1],
  [7,8,7]
]

graph_4 = [
  [0,1,4],
  [0,7,8],
  [1,2,8],
  [1,7,11],
  [1,8,5],
  [2,3,7],
  [2,5,2],
  [2,8,4],
  [3,4,9],
  [3,5,14],
  [3,6,6],
  [3,8,4],
  [4,5,10],
  [5,6,2],
  [6,8,6],
  [6,7,1],
  [7,8,7]
]

class Prims(object):

    def __init__(self):
        print("prim's")
        self.parent = dict()
        self.res = []

    def matrix(self,V, G):
      for i in range(0, V):
        self.res.append([])
        for j in range(0, V):
          self.res[i].append(0)

      for i in range(0, len(G)):
        self.res[G[i][0]][G[i][1]] = G[i][2]
        self.res[G[i][1]][G[i][0]] = G[i][2]

      return self.res

    def main(self,V, G):
      self.res = self.matrix(V, G)
      vertex = 0
      MST = []
      edges = []
      visited = []
      minEdge = [None,None,float('inf')]

      while len(MST) != V-1:
        visited.append(vertex)
        for r in range(0, V):
          if self.res[vertex][r] != 0:
            edges.append([vertex,r,self.res[vertex][r]])
        for e in range(0, len(edges)):
          if edges[e][2] < minEdge[2] and edges[e][1] not in visited:
            minEdge = edges[e]
        edges.remove(minEdge)
        MST.append(minEdge)
        vertex = minEdge[1]
        minEdge = [None,None,float('inf')]

      for i in range (0,len(MST)):
        print(MST[i])


if __name__ == '__main__':
    prims=Prims()
    prims.main(max(max(graph_1))+1,graph_1)
    print("\n")
    prims=Prims()
    prims.main(max(max(graph_2))+1,graph_2)
    print("\n")
    prims=Prims()
    prims.main(max(max(graph_3))+1,graph_3)
    print("\n")
    prims=Prims()
    prims.main(max(max(graph_4))+1,graph_4)
