# graph = {0: set([1, 2]),
#          1: set([3, 4]),
#          2: set([0, 4]),
#          3: set([1, 4]),
#          4: set([1, 2, 3, 5]),
#          5: set([3, 4])}

# graph_2 = {0: set([1, 2]),
#          1: set([3]),
#          2: set([3]),
#          3: set([])}

# graph = {0: set([1, 3, 4]),
#          1: set([2, 5]),
#          2: set([]),
#          3: set([6]),
#          4: set([6]),
#          5: set([]),
#          6: set([7]),
#          7: set([8]),
#          8: set([])}

# graph = {0: set([2, 3]),
#          1: set([0]),
#          2: set([1]),
#          3: set([4]),
#          4: set([])}

# graph_5 = {0: set([1, 2]),
#          1: set([3]),
#          2: set([3]),
#          3: set([])}

graph_6 = {1: set([2, 3, 4]),
           2: set([5, 6]),
           3: set([]),
           4: set([]),
           5: set([]),
           6: set([])}

output = []

def main(graph, start, visited=None):
    if visited is None:
        visited = set()
        output.append(start)
    visited.add(start)

    for next in sorted(graph[start] - visited):
        if (next not in output):
            output.append(next)
        main(graph, next, visited)
    return output

def main_paths(graph, start, goal, path=None):
    if path is None:
        path = [start]
    if start == goal:
        yield path
    for next in graph[start] - set(path):
        yield from main_paths(graph, next, goal, path + [next])

run= 1
graph = graph_6
print("all path")
print(main(graph, run))
print("\n")

print("path")
for i in range(min(graph)+1, max(graph)+1):
    print(list(main_paths(graph, run, i)))

    if ( i == min(graph)+1):
        all_1 = list(main_paths(graph, run, i))
    else:
        all_2 = list(main_paths(graph, run, i))
        if (len(all_1[0]) > len(all_2[0])):
            all_1 = all_2

print("\nshort part")
print(all_1[0])
