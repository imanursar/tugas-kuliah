graph_1 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11,1, 7),
(9, 3, 4),
(14,3, 5),
(10,4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

graph_2 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(5, 1, 8),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(14, 3, 5),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

graph_3 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(5, 1, 8),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(6, 3, 6),
(14, 3, 5),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

graph_4 = {
'nodes': [0, 1, 2, 3, 4, 5, 6, 7, 8],
'edges': [
(4, 0, 1),
(8, 1, 2),
(5, 1, 8),
(7, 2, 3),
(4, 2, 5),
(2, 2, 8),
(11, 1, 7),
(9, 3, 4),
(14, 3, 5),
(4, 3, 8),
(4, 5, 8),
(10, 4, 5),
(2, 5, 6),
(6, 6, 8),
(1, 6, 7),
(7, 7, 8),
(8, 0, 7)
]
}

class Kruskal(object):

    def __init__(self):
        print("kruskal\n")
        self.parent = dict()
        self.rank = dict()

    def set(self,node):
        self.parent[node] = node
        self.rank[node] = 0

    def find(self,node):
        if self.parent[node] != node:
            self.parent[node] = Kruskal.find(self,self.parent[node])
        return self.parent[node]

    def union(self,node1, node2):
        # rank = dict()
        root1 = Kruskal.find(self,node1)
        root2 = Kruskal.find(self,node2)
        if (root1 != root2):
            if self.rank[root1] > self.rank[root2]:
                self.parent[root2] = root1
            else:
                self.parent[root1] = root2
            if (self.rank[root1] == self.rank[root2]):
                self.rank[root2] += 1

    def main(self,graph):
        for node in graph['nodes']:
            Kruskal.set(self,node)
            mst = []

        edges = list(graph['edges'])
        edges.sort()

    	    #print edges
        for edge in edges:
            weight, node1, node2 = edge

            if Kruskal.find(self,node1) != Kruskal.find(self,node2):
                Kruskal.union(self,node1, node2)
                mst.append(edge)

        for i in range (0,len(mst)):
            print(mst[i])

if __name__ == '__main__':
    kruskal=Kruskal()
    kruskal.main(graph_1)
    print("\n")
    kruskal=Kruskal()
    kruskal.main(graph_2)
    print("\n")
    kruskal=Kruskal()
    kruskal.main(graph_3)
    print("\n")
    kruskal=Kruskal()
    kruskal.main(graph_4)

# print (kruskal(graph_2))
# print (kruskal(graph_3))
# print (kruskal(graph_4))
