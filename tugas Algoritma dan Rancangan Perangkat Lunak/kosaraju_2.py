from collections import defaultdict
import numpy as np

graph_1 = {0: [2,3],
         1: [0],
         2: [1],
         3: [4],
         4: []
         }

graph_2 = {0: [1],
           1: [2,3],
           2: [0],
           3: [4],
           4: [5],
           5: [3],
           6: [5,7],
           7: [8],
           8: [9],
           9: [6,10],
           10: []
        }

graph_3 = {0: [1],
           1: [2],
           2: [3,4],
           3: [0],
           4: [5],
           5: [6],
           6: [4,7],
           7: [8],
           8: []
        }

class Kosaraju(object):
    def __init__(self):
        print("\nKosaraju\n")
    def reverse(graph):
        new_graph = defaultdict(list)
        for u in graph:
            for v in graph[u]:
                new_graph[v].append(u)
        return new_graph

    def first(graph):
        visited = set()
        order = []

        def dfs(v):
            visited.add(v)
            for u in graph[v]:
                if u not in visited:
                    dfs(u)
            order.append(v)

        for u in graph.keys():
            if u not in visited:
                dfs(u)
        return order

    def second( graph, order):
        visited = set()
        front = defaultdict(list)
        for u in reversed(order):
            if u not in visited:
                visited.add(u)
                stack = [u]
                while stack:
                    item = stack.pop()
                    for v in graph[item]:
                        if v not in visited:
                            visited.add(v)
                            stack.append(v)
                    front[u].append(item)
        return front

    def main(self, graph):
        out = Kosaraju.second(graph, Kosaraju.first(Kosaraju.reverse(graph)))
        for key, value in out.items():
            print("start node: %d" %key ,end='')
            print("\tnetwork: ",end='')
            print(sorted(value))



if __name__ == '__main__':
    kosaraju = Kosaraju()
    kosaraju.main(graph_1)
    kosaraju = Kosaraju()
    kosaraju.main(graph_2)
    kosaraju = Kosaraju()
    kosaraju.main(graph_3)
