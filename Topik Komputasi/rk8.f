C===============================================================C
C	Programing of Numerical Computation			C
C								C
C		by M. Hirose	on 19-Sep-1989			C
C===============================================================C
*****************************************************************
*								*
*		  Test program of RUNGE				*
*								*
*		Read initial value from file 			*
*		  and store result to file			*
*								*
*****************************************************************
	PARAMETER (N = 2)
*
	EXTERNAL SAMPLE
*
	REAL*8 X0
	REAL*8 Y0(N),YN(N)
	REAL*8 DX,ERR
*
	REAL*8 W,R,D,MU,ONEMU,HALF,C0,C1
	PARAMETER (MU = 0.5D0, ONEMU = 1-MU, HALF = 0.5D0)
*
	DX = 1.0D0
	ICOMP = 1
*
	OPEN (7, FILE = 'RK8TEST.DAT',STATUS = 'NEW')
	OPEN (8, FILE = 'RK8PREC.DAT',STATUS = 'NEW')
*
	DO 10 K = 1, 6
*
	DX = DX / 2
	ICOMP = ICOMP * 2
*
	X0 = 0.0D0
	Y0(1) =  1.0D0
	Y0(2) =  0.0D0
*
	DO 30 I = 1, ICOMP
		X0 = X0 + DX
*
		CALL SHANKS (SAMPLE, X0, DX, Y0, YN)
*
		DO 31 J = 1, N
			Y0(J) = YN(J)
   31		CONTINUE
*
		ERR = (COS(X0)-Y0(1))/COS(X0)
		WRITE (*, 4000) X0,(Y0(J),J=1,N),ERR
		WRITE (7, 4000) X0,(Y0(J),J=1,N),ERR
 4000		FORMAT (1X,F8.5,1P2D23.15,D10.2)
*
   30	CONTINUE
*
	WRITE (*,4001) DX,ERR,LOG10(DX),LOG10(ABS(ERR))
	WRITE (8,4001) DX,ERR,LOG10(DX),LOG10(ABS(ERR))
 4001	FORMAT (1X,4F15.7)
*
   10	CONTINUE
*
	CLOSE (7)
*
	END
*
	SUBROUTINE SAMPLE (N,X,Y,F)
*****************************************************************
*								*
*		Subroutine for the 3 body problem		*
*								*
*****************************************************************
	REAL*8 X,Y(N),F(N)
*
	F(1) = -Y(2)
	F(2) =  Y(1)
*
	RETURN
*
	END
*
	SUBROUTINE SHANKS (SUB, X, H, Y, YN)
*****************************************************************
*								*
*		8-th order Runge-Kutta formula			*
*			by Shanks				*
*								*
*	Y(I) ...initial value at X				*
*	YN(I)...solution at X + H				*
*								*
*****************************************************************
	PARAMETER (N = 2)
	REAL*8 X,H,Y(N),YN(N)
	REAL*8 AK0(N),AK1(N),AK2(N),AK3(N),AK4(N),
     $	       AK5(N),AK6(N),AK7(N),AK8(N),AK9(N)
	REAL*8 W1(N),W2(N),W3(N),W4(N),W5(N),
     $	       W6(N),W7(N),W8(N),W9(N)
*
	REAL*8 ONE,TWO,THR,SIX,NIN,TWS
*
	REAL*8 AL1,AL2,AL3,AL4,AL5,AL6,AL7,AL8,AL9
	REAL*8 BD1,BD2,BD3,BD4,BD5,BD6,BD7,BD8,BD9
	REAL*8 B10
	REAL*8 B20,B21
	REAL*8 B30,B31,B32
	REAL*8 B40,B41,B42,B43
	REAL*8 B50,B51,B52,B53,B54
	REAL*8 B60,B61,B62,B63,B64,B65
	REAL*8 B70,B71,B72,B73,B74,B75,B76
	REAL*8 B80,B81,B82,B83,B84,B85,B86,B87
	REAL*8 B90,B91,B92,B93,B94,B95,B96,B97,B98
	REAL*8 GAD
	REAL*8 GA0,GA3,GA4,GA5,GA6,GA8,GA9
*
	PARAMETER (ONE = 1, TWO = 2, FOR = 4, FIF = 5)
*
	PARAMETER (AL1 = FOR / 27, AL2 = TWO / 9,  AL3 = ONE / 3,  
     $		   AL4 = ONE / 2,  AL5 = TWO / 3,  AL6 = ONE / 6,  
     $		   AL7 = ONE,      AL8 = FIF / 6,  AL9 = ONE)
*
	PARAMETER (BD1 = FOR / 27, BD2 = ONE / 18,  BD3 = ONE / 12, 
     $		   BD4 = ONE / 8,  BD5 = ONE / 54,  BD6 = ONE / 4320,
     $		   BD7 = ONE / 20, BD8 = ONE / 288, BD9 = ONE / 820)
*
	PARAMETER (B10 =    1*BD1,
     $		   B20 =    1*BD2, B21 =    3*BD2,
     $		   B30 =    1*BD3, B31 =    0,      B32 =    3*BD3,
     $		   B40 =    1*BD4, B41 =    0,      B42 =    0,
     $		   B43 =    3*BD4,
     $		   B50 =   13*BD5, B51 =    0,      B52 =  -27*BD5,
     $		   B53 =   42*BD5, B54 =    8*BD5,
     $		   B60 =  389*BD6, B61 =    0,      B62 =  -54*BD6,
     $		   B63 =  966*BD6, B64 = -824*BD6,  B65 =  243*BD6,
     $		   B70 = -231*BD7, B71 =    0,      B72 =   81*BD7,
     $		   B73 =-1164*BD7, B74 =  656*BD7,  B75 = -122*BD7,
     $		   B76 =  800*BD7,
     $		   B80 = -127*BD8, B81 =    0,      B82 =   18*BD8,
     $		   B83 = -678*BD8, B84 =  456*BD8,  B85 =   -9*BD8,
     $		   B86 =  576*BD8, B87 =    4*BD8,
     $		   B90 = 1481*BD9, B91 =    0,      B92 =  -81*BD9,
     $		   B93 = 7104*BD9, B94 =-3376*BD9,  B95 =   72*BD9,
     $		   B96 =-5040*BD9, B97 =  -60*BD9,  B98 =  720*BD9)
*
	PARAMETER (GAD = ONE / 840)
	PARAMETER (GA0 =   41*GAD, GA3 =   27*GAD,  GA4 =  272*GAD,
     $		   GA5 =   27*GAD, GA6 =  216*GAD,  GA8 =  216*GAD,
     $		   GA9 =   41*GAD)
*
	CALL SUB (N, X, Y, AK0)
*
	DO 810 I = 1, N
		AK0(I) = H * AK0(I)
		W1(I) = Y(I) + B10*AK0(I)
  810	CONTINUE
	CALL SUB (N, X+AL1*H, W1, AK1)
*
	DO 820 I = 1, N
		AK1(I) = H * AK1(I)
		W2(I) = Y(I) + B20*AK0(I) + B21*AK1(I)
  820	CONTINUE
	CALL SUB (N, X+AL2*H, W2, AK2)
*
	DO 830 I = 1, N
		AK2(I) = H * AK2(I)
		W3(I) = Y(I) + B30*AK0(I) + B31*AK1(I) + B32*AK2(I)
  830	CONTINUE
	CALL  SUB (N, X+AL3*H, W3, AK3)
*
	DO 840 I = 1, N
		AK3(I) = H * AK3(I)
		W4(I) = Y(I) + B40*AK0(I) + B41*AK1(I) + B42*AK2(I)
     $			     + B43*AK3(I)
  840	CONTINUE
	CALL SUB (N, X+AL4*H, W4, AK4)
*
	DO 850 I = 1, N
		AK4(I) = H * AK4(I)
		W5(I) = Y(I) + B50*AK0(I) + B51*AK1(I) + B52*AK2(I)
     $			     + B53*AK3(I) + B54*AK4(I)
  850	CONTINUE
	CALL SUB (N, X+AL5*H, W5, AK5)
*
	DO 860 I = 1, N
		AK5(I) = H * AK5(I)
		W6(I) = Y(I) + B60*AK0(I) + B61*AK1(I) + B62*AK2(I)
     $			     + B63*AK3(I) + B64*AK4(I) + B65*AK5(I)
  860	CONTINUE
	CALL SUB (N, X+AL6*H, W6, AK6)
*
	DO 870 I = 1, N
		AK6(I) = H * AK6(I)
		W7(I) = Y(I) + B70*AK0(I) + B71*AK1(I) + B72*AK2(I)
     $			     + B73*AK3(I) + B74*AK4(I) + B75*AK5(I)
     $			     + B76*AK6(I)
  870	CONTINUE
	CALL SUB (N, X+AL7*H, W7, AK7)
*
	DO 880 I = 1, N
		AK7(I) = H * AK7(I)
		W8(I) = Y(I) + B80*AK0(I) + B81*AK1(I) + B82*AK2(I)
     $			     + B83*AK3(I) + B84*AK4(I) + B85*AK5(I)
     $			     + B86*AK6(I) + B87*AK7(I)
  880	CONTINUE
	CALL SUB (N, X+AL8*H, W8, AK8)
*
	DO 890 I = 1, N
		AK8(I) = H * AK8(I)
		W9(I) = Y(I) + B90*AK0(I) + B91*AK1(I) + B92*AK2(I)
     $			     + B93*AK3(I) + B94*AK4(I) + B95*AK5(I)
     $			     + B96*AK6(I) + B97*AK7(I) + B98*AK8(I)
  890	CONTINUE
	CALL SUB (N, X+AL9*H, W9, AK9)
*
	DO 900 I = 1, N
		AK9(I) = H * AK9(I)
		YN(I) = Y(I) + GA0*AK0(I) + GA3*AK3(I) + GA4*AK4(I)
     $			     + GA5*AK5(I) + GA6*AK6(I) + GA8*AK8(I) 
     $			     + GA9*AK9(I)
  900	CONTINUE
*
	RETURN
	END

