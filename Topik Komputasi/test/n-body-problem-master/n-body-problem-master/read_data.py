from __future__ import division
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from numpy import genfromtxt

a = genfromtxt("sun_x.csv", delimiter=',')
b = genfromtxt("sun_y.csv", delimiter=',')
c = genfromtxt("sun_z.csv", delimiter=',')
sun = np.zeros((len(a), 	3))
sun[:,0] = a
sun[:,1] = b
sun[:,2] = c

a = genfromtxt("earth_x.csv", delimiter=',')
b = genfromtxt("earth_y.csv", delimiter=',')
c = genfromtxt("earth_z.csv", delimiter=',')
earth = np.zeros((len(a), 	3))
earth[:,0] = a
earth[:,1] = b
earth[:,2] = c

a = genfromtxt("Mercury_x.csv", delimiter=',')
b = genfromtxt("Mercury_y.csv", delimiter=',')
c = genfromtxt("Mercury_z.csv", delimiter=',')
Mercury = np.zeros((len(a), 	3))
Mercury[:,0] = a
Mercury[:,1] = b
Mercury[:,2] = c
#
a = genfromtxt("Venus_x.csv", delimiter=',')
b = genfromtxt("Venus_y.csv", delimiter=',')
c = genfromtxt("Venus_z.csv", delimiter=',')
Venus = np.zeros((len(a), 	3))
Venus[:,0] = a
Venus[:,1] = b
Venus[:,2] = c
#
a = genfromtxt("Mars_x.csv", delimiter=',')
b = genfromtxt("Mars_y.csv", delimiter=',')
c = genfromtxt("Mars_z.csv", delimiter=',')
Mars = np.zeros((len(a), 	3))
Mars[:,0] = a
Mars[:,1] = b
Mars[:,2] = c

# a = genfromtxt("Jupyter_x.csv", delimiter=',')
# b = genfromtxt("Jupyter_y.csv", delimiter=',')
# c = genfromtxt("Jupyter_z.csv", delimiter=',')
# Jupyter = np.zeros((len(a), 	3))
# Jupyter[:,0] = a
# Jupyter[:,1] = b
# Jupyter[:,2] = c
#
# a = genfromtxt("Saturn_x.csv", delimiter=',')
# b = genfromtxt("Saturn_y.csv", delimiter=',')
# c = genfromtxt("Saturn_z.csv", delimiter=',')
# Saturnus = np.zeros((len(a), 	3))
# Saturnus[:,0] = a
# Saturnus[:,1] = b
# Saturnus[:,2] = c
#
# a = genfromtxt("Neptunus_x.csv", delimiter=',')
# b = genfromtxt("Neptunus_y.csv", delimiter=',')
# c = genfromtxt("Neptunus_z.csv", delimiter=',')
# Neptunus = np.zeros((len(a), 	3))
# Neptunus[:,0] = a
# Neptunus[:,1] = b
# Neptunus[:,2] = c
#
# a = genfromtxt("Uranus_x.csv", delimiter=',')
# b = genfromtxt("Uranus_y.csv", delimiter=',')
# c = genfromtxt("Uranus_z.csv", delimiter=',')
# Uranus = np.zeros((len(a), 	3))
# Uranus[:,0] = a
# Uranus[:,1] = b
# Uranus[:,2] = c
#
# a = genfromtxt("Pluto_x.csv", delimiter=',')
# b = genfromtxt("Pluto_y.csv", delimiter=',')
# c = genfromtxt("Pluto_z.csv", delimiter=',')
# Pluto = np.zeros((len(a), 	3))
# Pluto[:,0] = a
# Pluto[:,1] = b
# Pluto[:,2] = c

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(sun[:,0], sun[:,1], sun[:,2], label='SUN', lw=2, color='black')
ax.plot(earth[:,0], earth[:,1], earth[:,2], lw=2, label = "Earth", color="blue")
ax.plot(Mercury[:,0], Mercury[:,1], Mercury[:,2], lw=2, label = "Mercury", color="gray")
ax.plot(Venus[:,0], Venus[:,1], Venus[:,2], lw=2, label = "Venus", color="yellow")
ax.plot(Mars[:,0], Mars[:,1], Mars[:,2], lw=2, label = "Mars", color="red")

# ax.plot(Uranus[:,0], Uranus[:,1], Uranus[:,2], lw=2, label = "Uranus", color="red")
# ax.plot(Jupyter[:,0], Jupyter[:,1], Jupyter[:,2], lw=2, label = "Jupyter", color="red")
# ax.plot(Saturnus[:,0], Saturnus[:,1], Saturnus[:,2], lw=2, label = "Saturnus", color="red")
# ax.plot(Neptunus[:,0], Neptunus[:,1], Neptunus[:,2], lw=2, label = "Neptunus", color="red")
# ax.plot(Pluto[:,0], Pluto[:,1], Pluto[:,2], lw=2, label = "Pluto", color="black")
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=3, mode="expand", borderaxespad=0.)
ax.set_xlim3d(-3e11, 3e11)
ax.set_ylim3d(-3e11, 3e11)
ax.set_zlim3d(-3e11, 3e11)

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(earth[:,0],'-b', label='x')
ax1.plot(earth[:,1],'-r', label='y')
ax1.plot(earth[:,2],'-g', label='z')

coo = np.zeros((len(sun)))
for i in range(len(sun)):
	coo[i] = ((earth[i,0]-sun[i,0])**2 + (earth[i,1]-sun[i,1])**2 + (earth[i,2]-sun[i,2])**2)**0.5

fig = plt.figure()
ax2 = fig.add_subplot(111)
ax2.plot(coo,'-b', label='x')

np.savetxt("coo.csv", coo, delimiter=",")

plt.show()
