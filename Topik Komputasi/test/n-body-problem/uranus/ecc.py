import numpy as np
import matplotlib.pyplot as plt

from numpy import genfromtxt
a = genfromtxt('r.csv', delimiter=',')

fig = plt.figure()
ax2 = fig.add_subplot(111)
ax2.plot(a,'-b', label='x')
# plt.show()

n = 100
k = 0
r = np.zeros((n,2))
e = np.zeros((n,1))
am = np.zeros((n,1))

t = 365*84
# t = 840
print (t)

for i in range (0,100):
    r[i,0] = (min(a[k:k+t]))
    r[i,1] = (max(a[k:k+t]))
    k += t
    e[i,0] = (r[i,1] - r[i,0])/(r[i,1] + r[i,0])
    am[i,0] = (r[i,1] + r[i,0])/2

print(np.mean(e))
print(np.mean(am))

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(e,'-b', label='e')

fig = plt.figure()
ax2 = fig.add_subplot(111)
ax2.plot(am,'-b', label='a')

plt.show()
