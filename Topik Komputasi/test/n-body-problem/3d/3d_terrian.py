from matplotlib import pyplot as plt
import numpy as np
import mpl_toolkits.mplot3d.axes3d as p3
from matplotlib import animation
from numpy import genfromtxt

a = genfromtxt("Mercury_x.csv", delimiter=',')
solar = np.zeros((5,3,len(a)))
solar[0,0,:] = genfromtxt("sun_x.csv", delimiter=',')
solar[0,1,:] = genfromtxt("sun_y.csv", delimiter=',')
solar[0,2,:] = genfromtxt("sun_z.csv", delimiter=',')
solar[1,0,:] = genfromtxt("Mercury_x.csv", delimiter=',')
solar[1,1,:] = genfromtxt("Mercury_y.csv", delimiter=',')
solar[1,2,:] = genfromtxt("Mercury_z.csv", delimiter=',')
solar[2,0,:] = genfromtxt("Venus_x.csv", delimiter=',')
solar[2,1,:] = genfromtxt("Venus_y.csv", delimiter=',')
solar[2,2,:] = genfromtxt("Venus_z.csv", delimiter=',')
solar[3,0,:] = genfromtxt("earth_x.csv", delimiter=',')
solar[3,1,:] = genfromtxt("earth_y.csv", delimiter=',')
solar[3,2,:] = genfromtxt("earth_z.csv", delimiter=',')
solar[4,0,:] = genfromtxt("Mars_x.csv", delimiter=',')
solar[4,1,:] = genfromtxt("Mars_y.csv", delimiter=',')
solar[4,2,:] = genfromtxt("Mars_z.csv", delimiter=',')

N_trajectories = 5

# Set up figure & 3D axis for animation
fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1], projection='3d')
ax.axis('off')

# choose a different color for each trajectory
colors = plt.cm.jet(np.linspace(0, 1, N_trajectories))

# set up lines and points
lines = sum([ax.plot([], [], [], '-', c=c)
             for c in colors], [])
pts = sum([ax.plot([], [], [], 'o', c=c)
           for c in colors], [])

# prepare the axes limits
ax.set_xlim((-25, 25))
ax.set_ylim((-35, 35))
ax.set_zlim((5, 55))

# set point-of-view: specified by (altitude degrees, azimuth degrees)
ax.view_init(30, 0)

# initialization function: plot the background of each frame
def init():
    for line, pt in zip(lines, pts):
        line.set_data([], [])
        line.set_3d_properties([])

        pt.set_data([], [])
        pt.set_3d_properties([])
    return lines + pts

# def update(i, sun, mercury, line_sun, pts_sun, line_mer, pts_mer):
#     line.set_data(mercury[:2, :num])
#     line.set_3d_properties(mercury[2, :num])
#     pts.set_data(mercury[:2, num-1:num])
#     pts.set_3d_properties(mercury[2, num-1:num])
# def animate(i):
#     we'll step two time-steps per frame.  This leads to nice results.
#     i = (5 * i) % mercury.shape[1]
#
#     for line, pt, xi in zip(lines, pts, x_t):
#     x = mercury[0,:i]
#     y = mercury[1,:i]
#     z = mercury[2,:i]
#     line.set_data(x, y)
#     line.set_3d_properties(z)
#
#     pts.set_data(x[-1:], y[-1:])
#     pts.set_3d_properties(z[-1:])
#     ax.view_init(30, 0.3 * i)
#     fig.canvas.draw()
#     return lines + pts

# animation function.  This will be called sequentially with the frame number
def animate(i):
    # we'll step two time-steps per frame.  This leads to nice results.
    i = (5 * i) % solar.shape[2]
    n = 0
    for line, pt, xi in zip(lines, pts, solar):
        x = solar[n,0,:i]
        y = solar[n,1,:i]
        z = solar[n,2,:i]
        line.set_data(x, y)
        line.set_3d_properties(z)

        pt.set_data(x[-1:], y[-1:])
        pt.set_3d_properties(z[-1:])
        n += 1

    # ax.view_init(30, 0.3 * i)
    fig.canvas.draw()
    return lines + pts

# initialization function: plot the background of each frame
def init():
    for line, pt in zip(lines, pts):
        line.set_data([], [])
        line.set_3d_properties([])

        pt.set_data([], [])
        pt.set_3d_properties([])
    return lines + pts

# Setting the axes properties
ax.set_xlim3d(-2e11, 2e11)
ax.set_ylim3d(-2e11, 2e11)
ax.set_zlim3d(-2e11, 2e11)

ani = animation.FuncAnimation(fig, animate, init_func=init, interval=0.1, blit=False, frames= 1000)

ani.save('matplot003.gif', writer='imagemagick')
# ani.save('line_animation_3d_funcanimation.mp4', writer='ffmpeg')
# plt.show()
