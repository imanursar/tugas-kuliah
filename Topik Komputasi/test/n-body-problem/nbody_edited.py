from __future__ import division
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

COLORS = ['b', 'g', 'r', 'y', 'm', 'k', 'c']

G = 6.67259e-11 # gravitational constant

class Particle(object):

	def __init__(self, name, mass, r0, v0):
		self.name = name
		self.mass = mass
		self.r0 = r0
		self.v0 = v0

def read_particles_data(file_name):
	global NAMES, MASSES, POS_0, VEL_O, PLANETS
	def _format_vector(vec):
		"""
			Utility function that formats an input string
			in the format: '%f;%f' into a tuple (x, y)

			::vec:: input string
			::return:: vector as tuple
		"""
		xy = vec.split(';')
		return (float(xy[0].strip()), float(xy[1].strip()), float(xy[2].strip()))

	data = []

	with open(file_name, 'r') as file:
		content = file.read()
		particles = content.split('\n\n')

		for part in particles:
			p = part.split('\n')
			name = p[0].strip()
			mass = float(p[1].strip())
			r0 = _format_vector(p[2].strip())
			# print(r0)
			v0 = _format_vector(p[3].strip())

			data.append(Particle(name, mass, r0, v0))

	return data

def draw(data, STEPS, T_MAX):
	PARTICLES = len(data)
	h = T_MAX / STEPS # step size

	m = np.zeros(PARTICLES) # masses
	#			  time	 	planet     x,y,z
	r = np.zeros((STEPS, 	PARTICLES, 	3)) # positions
	#				planet   v_x,v_y,v_z
	v_s = np.zeros((PARTICLES,	3)) # velocities at step s

	# apply initial conditions
	for p in range(PARTICLES):
		m[p] = data[p].mass
		for i in range(3):
			r[0][p][i] = data[p].r0[i]
			v_s[p][i] = data[p].v0[i]

	def mod3(v):
		""" returns the lenght of a 2d-vector to the third power """
		return np.power(v[0]*v[0] + v[1]*v[1] + v[2]*v[2], 1.5)

	def g(p, rs):
		"""
			return the intensity of the gravitational force in particle p due to the others
				given by Newton's law of universal gravitation:
					g_p = - G * sum_k [m_k * (r_p - r_k) / |r_p - r_k|^2 where k != p]
		"""
		v = [0, 0, 0]
		for k in range(PARTICLES):
			if k == p:
				continue

			v += m[k] * (rs[p] - rs[k]) / mod3(rs[p] - rs[k])

		return - G * v

################################################################################
#### Runge-Kutta 8th Order
################################################################################

	rk4 = np.zeros((11, PARTICLES, 3))
	for s in range(1, STEPS):
		for p in range(PARTICLES):
			rk4[1][p] = g(p, r[s - 1])
		for p in range(PARTICLES):
			rk4[2][p] = g(p, r[s - 1] + (h*4/27) * rk4[1])
		for p in range(PARTICLES):
			rk4[3][p] = g(p, r[s - 1] + (h/18) * (rk4[1] + 3 * rk4[2]))
		for p in range(PARTICLES):
			rk4[4][p] = g(p, r[s - 1] + (h/12) * (rk4[1] + 3 * rk4[3]))
		for p in range(PARTICLES):
			rk4[5][p] = g(p, r[s - 1] + (h/8) * (rk4[1] + 3 * rk4[4]))
		for p in range(PARTICLES):
			rk4[6][p] = g(p, r[s - 1] + (h/54) * (13*rk4[1] - 27*rk4[3] + 42*rk4[4] + 8*rk4[5]))
		for p in range(PARTICLES):
			rk4[7][p] = g(p, r[s - 1] + (h/4320) * (389*rk4[1]-54*rk4[3]+966*rk4[4]-824*rk4[5]+243*rk4[6]))
		for p in range(PARTICLES):
			rk4[8][p] = g(p, r[s - 1] + (h/20) * (-234*rk4[1]+81*rk4[3]-1164*rk4[4]+656*rk4[5]-122*rk4[6]+800*rk4[7]))
		for p in range(PARTICLES):
			rk4[9][p] = g(p, r[s - 1] + (h/288) * (-127*rk4[1]+18*rk4[3]-678*rk4[4]+456*rk4[5]-9*rk4[6]+576*rk4[7]+4*rk4[8]))
		for p in range(PARTICLES):
			rk4[10][p] = g(p, r[s - 1] + (h/820) * (1481*rk4[1]-81*rk4[3]+7104*rk4[4]-3376*rk4[5]+72*rk4[6]-5040*rk4[7]-60*rk4[8]+720*rk4[9]))

		v_s += (h / 840) * ((41 * rk4[1]) + (27 * rk4[4]) + (272 * rk4[5]) + (27 * rk4[6]) + (216 * rk4[7]) + (216*rk4[9]) + (41*rk4[10]))
		r[s] = r[s - 1] + h * v_s

	# plot the trajectory
	# for p in range(PARTICLES):

	# fig = plt.figure()
	# ax = fig.add_subplot(111, projection='3d')
	# ax.plot(r[:,0,0], r[:,0,1], r[:,0,2], label='SUN', lw=2, color='black')

	# for p in loop:
	# # for p in range(PARTICLES):
	# 	ax.plot(r[:,p,0], r[:,p,1], r[:,p,2], label=data[p].name, lw=2, color=COLORS[p % len(COLORS)])
	# #
	# ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=3, mode="expand", borderaxespad=0.) # legend location at top

	np.savetxt("sun_x.csv", r[:,0,0], delimiter=",")
	np.savetxt("sun_y.csv", r[:,0,1], delimiter=",")
	np.savetxt("sun_z.csv", r[:,0,2], delimiter=",")

	# np.savetxt("earth_x.csv", r[:,1,0], delimiter=",")
	# np.savetxt("earth_y.csv", r[:,1,1], delimiter=",")
	# np.savetxt("earth_z.csv", r[:,1,2], delimiter=",")
	#
	# np.savetxt("Mercury_x.csv", r[:,2,0], delimiter=",")
	# np.savetxt("Mercury_y.csv", r[:,2,1], delimiter=",")
	# np.savetxt("Mercury_z.csv", r[:,2,2], delimiter=",")
	# #
	# np.savetxt("Venus_x.csv", r[:,3,0], delimiter=",")
	# np.savetxt("Venus_y.csv", r[:,3,1], delimiter=",")
	# np.savetxt("Venus_z.csv", r[:,3,2], delimiter=",")
	#
	# np.savetxt("Mars_x.csv", r[:,4,0], delimiter=",")
	# np.savetxt("Mars_y.csv", r[:,4,1], delimiter=",")
	# np.savetxt("Mars_z.csv", r[:,4,2], delimiter=",")

	np.savetxt("Jupyter_x.csv", r[:,1,0], delimiter=",")
	np.savetxt("Jupyter_y.csv", r[:,1,1], delimiter=",")
	np.savetxt("Jupyter_z.csv", r[:,1,2], delimiter=",")


	np.savetxt("Saturn_x.csv", r[:,2,0], delimiter=",")
	np.savetxt("Saturn_y.csv", r[:,2,1], delimiter=",")
	np.savetxt("Saturn_z.csv", r[:,2,2], delimiter=",")

	np.savetxt("Uranus_x.csv", r[:,3,0], delimiter=",")
	np.savetxt("Uranus_y.csv", r[:,3,1], delimiter=",")
	np.savetxt("Uranus_z.csv", r[:,3,2], delimiter=",")

	np.savetxt("Neptunus_x.csv", r[:,4,0], delimiter=",")
	np.savetxt("Neptunus_y.csv", r[:,4,1], delimiter=",")
	np.savetxt("Neptunus_z.csv", r[:,4,2], delimiter=",")

	np.savetxt("Pluto_x.csv", r[:,5,0], delimiter=",")
	np.savetxt("Pluto_y.csv", r[:,5,1], delimiter=",")
	np.savetxt("Pluto_z.csv", r[:,5,2], delimiter=",")
	# set x, y plot limits for convenience
	# ax.set_xlim3d(-0.75e13, 0.75e13)
	# ax.set_ylim3d(-0.75e13, 0.75e13)
	# ax.set_zlim3d(-0.75e13, 0.75e13)
	# plt.savefig("nbody.pdf")
	# fig = plt.figure()
	# ax1 = fig.add_subplot(111)
	# ax1.plot(r[:,det,0],'-b', label='x')
	# ax1.plot(r[:,det,1],'-r', label='y')
	# ax1.plot(r[:,det,2],'-g', label='z')
	# ax1.legend(loc='upper right')

	# print(len(r))
	# print(r.shape)
	# a = np.zeros((len(r)))
	# for i in range(len(r)):
	# 	a[i] = ((r[i,det,0]-r[i,0,0])**2 + (r[i,det,1]-r[i,0,1])**2 + (r[i,det,2]-r[i,0,2])**2)**0.5
	#
	# fig = plt.figure()
	# ax2 = fig.add_subplot(111)
	# ax2.plot(a,'-b', label='x')
	# plt.show()

	# np.savetxt("r.csv", a, delimiter=",")
	# np.savetxt("xyz.csv", r, delimiter=",")

if __name__ == '__main__':
	file_name = None
	if len(sys.argv) > 1:
		file_name = sys.argv[1]
	else:
		file_name = 'ss_data.txt'

	data = read_particles_data(file_name)
	draw(data, 365000, 300 * 365 * 24 * 3600)
